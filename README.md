# Brex matcher
Brex matcher is java library allows you text analyses using keywords good known in PPC campaigns like GoogleAds. Name was created from BRoad and EXact keywods. It is based on 4 types of keyword.

## Theory
### Keyword / match types

| Type | Start symbol | End symbol | Meaning |
|------|--------------|------------|---------|  
| **Broad** | none (default) | none (default) | broad is default type and requires only all parts have to be found in input text | 
| **Phrase** | " (double quote) | " (double quote) | phrase requires all parts in right order and allows words around |
| **Exact** | [ (square bracket) | ] (square bracket) | exact requires right order and nothing more in input string |
| **Negative** | - (minus sign) | none | if negative keyword matches, whole match fails |

### Broad match modifier
Broad match modifier with start symbol + (plus sign) has little different meaning from PPC. It is used for keyword part of any match and changes match to precise. Internally brex matches parts without care of letter case and tolerates no accents alternative. Modifier disables it, so it is more strict.

## Why to use?
Brex can help you with text analyse to reliable detect something by well prepared keywords. I am using it for detection of right board game from ad title.

### Examples

#### Exact match
Keywords:

    [7 divů světa]
    [7 wonders]
| Input | Matches | Comment |
|-------|:-------:|---------|
| `"7 divů světa"` | true | exact match |
| `"7 wonders"` | true | exact match 2nd keyword |
| `"7 Wonders"` | true | exact match 2nd keyword, case insensitive |
| `"7 Divu Sveta"` | true | exact match 1st keyword, case insensitive, accents tolerate |
| `"7divů světa"` | false | missing space |
| `"7 divů"` | false | missing last word |
| `"7wonders"` | false | missing space and last word |
| `"7 wonders Babel"` | false | word after not allowed |

#### Phrase match
Keywords:

    "Krycí jména"
    Codenames
    -XXL
| Input | Matches | Comment |
|-------|:-------:|---------|
| `"Duet (Krycí jména)"` | true | phrase match 1st keyword, foreign symbols ignored |
| `"codenames Duet"` | true | phrase match 2nd keyword, case insensitive |
| `"Kryci jmena"` | true | exact match 1nd keyword, accents tolerate |
| `"codenames: pictures"` | true | exact match 2nd keyword, accents tolerate, foreign symbols ignored |
| `"Krycí jména XXL"` | false | exact match 1st keyword but negative match 3rd |
| `"Jména Krycí"` | false | wrong order |

#### Broad match
Keywords:

    Carcassonne expansion
    Carcasone expansion
    Carcasonne expansion
    Carcassone expansion
    -"big box"
    -bigbox
| Input | Matches | Comment |
|-------|:-------:|---------|
| `"Expansion 3: Carcassonne - The Princess & The Dragon"` | true | broad match 1st keyword |
| `"Tower (Expansion for Carcasone)"` | true | broad match 2nd keyword |
| `"Carcasonne BigBox"` | false | broad match 3rd keyword but negative 6st |
| `"Carcassone Big box"` | false | broad match 4rd keyword but negative 5st |
| `"Carcassone"` | false | not all parts found |

#### Broad match modifier
Keywords:

    +háčkovat
| Input | Matches | Comment |
|-------|:-------:|---------|
| `"Já se učím hackovat jako Kevin Mitnick!"` | false | broad match modifier requires precise match | 
| `"To mě naučila háčkovat babička."` | true | broad match with modifier succeeded |

#### Only negative match
Keywords:

    -idiot
    -fool
    -shit
    -fuck
    -motherfucker
    -stupid
| Input | Matches | Comment |
|-------|:-------:|---------|
| `"I love everybody."` | true | only negative keywords and all not matched (special situation) |
| `"It is stupid. You are crazy idiot."` | false | Negative match 1st and 6th |

## Are you interested
Great! Feel free to contact me. 