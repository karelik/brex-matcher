package cz.ulman.brex;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class KeywordsTest {
    @Test
    public void testExact(){
        final Keywords kws = new Keywords("[7 divů světa]", "[7 wonders]");

        assertTrue(kws.matches("7 divů světa", true));
        assertTrue(kws.matches("7 wonders", true));
        assertTrue(kws.matches("7 Wonders", true));
        assertTrue(kws.matches("7 Divu Sveta", true));

        assertFalse(kws.matches("7divů světa", true));
        assertFalse(kws.matches("7 divů", true));
        assertFalse(kws.matches("7wonders", true));
        assertFalse(kws.matches("7 wonders Babel", true));
    }

    @Test
    public void testPhrase(){
        final Keywords kws = new Keywords("\"Krycí jména\"","Codenames","-XXL");
        assertTrue(kws.matches("Duet (Krycí jména)", true));
        assertTrue(kws.matches("codenames Duet", true));
        assertTrue(kws.matches("Kryci jmena", true));
        assertTrue(kws.matches("codenames: pictures", true));

        assertFalse(kws.matches("Krycí jména XXL", true));
        assertFalse(kws.matches("Jména Krycí", true));
    }

    @Test
    public void testBroad(){
        final Keywords kws = new Keywords("Carcassonne expansion","Carcasone expansion","Carcasonne expansion","Carcassone expansion","-\"big box\"","-bigbox");
        assertTrue(kws.matches("Expansion 3: Carcassonne - The Princess & The Dragon", true));
        assertTrue(kws.matches("Tower (Expansion for Carcasone)", true));
        assertFalse(kws.matches("Carcasonne BigBox", true));
        assertFalse(kws.matches("Carcassone Big box", true));
        assertFalse(kws.matches("Carcassone", true));
    }

    @Test
    public void testPreciseEquals(){
        final Keywords kws = new Keywords("+háčkovat");
        //https://www.idnes.cz/technet/technika/hacky-a-carky-delaji-problemy-i-po-600-letech.A080117_180422_tec_technika_pka
        assertFalse(kws.matches("Já se učím hackovat jako Kevin Mitnick!", true));
        assertTrue(kws.matches("To mě naučila háčkovat babička.", true));
        // TODO more english one :)
    }

    @Test
    public void testSort(){
        final Keywords kws = new Keywords("-[test]","\"otestovat aplikaci\"","[java aplikaci]","junit","-\"test motoru\"","-diagnostika","-[test aplikační]");
        assertEquals("Keywords:\n-[test aplikační]\n-[test]\n-\"test motoru\"\n-diagnostika\n[java aplikaci]\n\"otestovat aplikaci\"\njunit\n", kws.toString());
    }

    @Test
    public void testNegativesOnly(){
        final Keywords kws = new Keywords("-idiot","-fool","-shit","-fuck","-motherfucker","-stupid");
        assertTrue(kws.matches("I love everybody.", true));
        assertFalse(kws.matches("It is stupid. You are crazy idiot.", true));
    }
    @Test
    public void test(){
        final Keywords kws = new Keywords("test");
        final Keyword kw = kws.getKeywords().get(0);
        assertEquals(Keyword.Type.EXACT, kw.matchDetect("test"));
        assertEquals(Keyword.Type.PHRASE, kw.matchDetect("test java"));
    }
}
