package cz.ulman.brex;

import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

// TODO maybe apply string distance for tiny variations, misspellings
/**
 * One "word" of keyword.
 */
@Getter
public class Part {
    /**
     * Symbol you can add to begin of word to mark word match exactly like you write it (cases, diacritics etc.)
     */
    public static final String PRECISE_EQUALS_SYMBOL = "+";
    /**
     * Stores string variants to simple search (without accents only now)
     */
    private final List<String> textVariants = new ArrayList<>(2);
    /**
     * If precise mode is enabled.
     */
    private final boolean preciselyEquals;

    public Part(String text) {
        this.preciselyEquals = text.startsWith(PRECISE_EQUALS_SYMBOL);
        final String processed = preciselyEquals ? text.substring(1) : text;
        this.textVariants.add(processed);

        if (!preciselyEquals) {
            final String strippedAccents = StringUtils.stripAccents(processed);
            if (!textVariants.contains(strippedAccents))
                this.textVariants.add(strippedAccents);
        }
    }

    /**
     * If text matches this part.
     * @param text
     * @return
     */
    public boolean matches(String text){
        final String strippedAccentsText = StringUtils.stripAccents(text);
        return textVariants.stream()
                .anyMatch(str -> str.equals(text) || (!preciselyEquals && (str.equalsIgnoreCase(text) || str.equalsIgnoreCase(strippedAccentsText))));
    }

    public String toString(){
        return (preciselyEquals ? PRECISE_EQUALS_SYMBOL : "") + textVariants.get(0);
    }
}
