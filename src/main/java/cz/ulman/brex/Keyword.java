package cz.ulman.brex;

import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;
import java.util.stream.*;

@Getter
public class Keyword {
    /**
     * Collection of keyword parts
     */
    private final List<Part> parts;
    /**
     * Type - broad, phrase or exact.
     */
    private final Type type;
    /**
     * Negativity
     */
    private final boolean negative;

    public Keyword(String text) {
        final List<Type> types = Type.getType(text);
        this.negative = types.remove(Type.NEGATIVE);
        this.type = types.get(0);
        this.parts = prepareParts(text);
    }

    private List<Part> prepareParts(String text) {
        final int begin = (negative ? 1 : 0) + type.getStartSymbol().length();
        final int end = text.length() - type.getEndSymbol().length();
        return Stream.of(StringUtils.split(text.substring(begin, end),' '))
                .map(Part::new)
                .collect(Collectors.toList());
    }

    /**
     * If text matches this keyword.
     * Test if detected keyword type is at least type.
     * E.g. for BROAD keywords is suitable BROAD, PHRASE and EXACT result, for PHRASE is suitable PHRASE, EXACT
     * @param text
     * @return
     */
    public boolean matches(String text) {
        final Type detectedType = matchDetect(text);
        if (detectedType == null) return false;
        return detectedType.getSortValue() >= type.getSortValue();
    }

    /**
     * Detects match type of text with this keyword
     * @param text
     * @return
     */
    public Keyword.Type matchDetect(String text) {
        final String[] inputParts = StringUtils.split(text, ' ');
        final Part[] foundParts = new Part[inputParts.length];

        for (Part part : parts){
            boolean found = false;
            for (int i = 0; i < inputParts.length; i++){
                final String inputPart = inputParts[i];
                if (part.matches(inputPart)){
                    found = true;
                    foundParts[i] = part;
                }
            }
            if (!found) return null; // not found part => no match
        }
        // all parts were found
        if (parts.size() == 1) {
            if (inputParts.length == 1)
                return Keyword.Type.EXACT;
            return Keyword.Type.PHRASE; // or BROAD, hard to decide
        }

        // more parts - detect chain
        Integer firstIndex = null;
        Integer lastIndex = null;
        boolean chainOk = false;
        final LinkedList<Part> partsToProcess = new LinkedList<>(parts);
        final LinkedList<Integer> firstIndexes = getIndexes(foundParts, partsToProcess.removeFirst());
        final Iterator<Integer> it = firstIndexes.iterator();
        while(it.hasNext()){
            chainOk = true;
            firstIndex = it.next();
            lastIndex = firstIndex;
            for (Part part : partsToProcess){ // can be empty
                int index = ArrayUtils.indexOf(foundParts, part, lastIndex + 1); // search in rest of array
                if (index != lastIndex + 1) {
                    // not next item, fail
                    firstIndex = null;
                    lastIndex = null;
                    chainOk = false;
                    it.remove();
                    break;
                }
                lastIndex = index;
            }
            if (chainOk) break; // success, no fail detected
        }

        if (chainOk && firstIndex == 0 && lastIndex == foundParts.length - 1){
            return Keyword.Type.EXACT;
        } else if (chainOk) {
            return Keyword.Type.PHRASE;
        } else {
            return Keyword.Type.BROAD;
        }
    }

    /**
     * Returns all indexes of part in foundParts.
     * @param foundParts
     * @param part
     * @return
     */
    private static LinkedList<Integer> getIndexes(Part[] foundParts, Part part) {
        final LinkedList<Integer> indexes = new LinkedList<>();
        int i = -1;
        int index;
        do {
            index = ArrayUtils.indexOf(foundParts, part, i + 1);
            if (index > -1) {
                indexes.add(index);
                i = index;
            }
        } while (index > -1);
        return indexes;
    }

    /**
     * Sort value, primary is type so it is weighted to 100. Second criteria is word count;
     * So result descending should be e. g.:
     * NEGATIVE EXACT 2 words
     * NEGATIVE EXACT 1 words
     * NEGATIVE PHRASE 3 words
     * NEGATIVE BROAD 4 words
     * EXACT 5 words
     * PHRASE 6 words
     * BROAD 7 words
     * @return
     */
    public int getSortValue(){
        int typeSum = 0;
        if (negative) typeSum += Type.NEGATIVE.getSortValue();
        typeSum += type.getSortValue();
        typeSum *= 100; // more weight

        typeSum += parts.size();
        return typeSum;
    }

    public String toString(){
        final StringBuilder str = new StringBuilder();
        if (negative) str.append(Type.NEGATIVE.getStartSymbol());

        str.append(type.getStartSymbol());
        for (Part p : parts){
            str.append(p.toString()).append(" ");
        }
        str.deleteCharAt(str.length() - 1);

        str.append(type.getEndSymbol());
        return str.toString();
    }

    /**
     * Keyword types enum. Don't change order of items (getType(String))
     */
    @Getter
    @AllArgsConstructor
    public enum Type {
        /**
         * If any negative keyword is found in text, it is not matched.
         */
        NEGATIVE("-", "", false, 4),
        /**
         * Exact keyword is matched only if text contains all parts in order and nothing more.
         */
        EXACT("[", "]", true, 3),
        /**
         * Phrase keyword is matched if text contains all parts in order. Text can contain something more around.
         */
        PHRASE("\"", "\"", true, 2),
        /**
         * Broad keyword is matched if text contains all parts only. Doesn't matter order included words etc.
         */
        BROAD("", "", true, 1),
        ;
        private final String startSymbol;
        private final String endSymbol;
        /**
         * If type is unique not to continue detect another. All types are finite except negative.
         * Negative can be combined with rest so is not finite.
         */
        private final boolean finite;
        /**
         * Some kind of importancy value.
         */
        private final int sortValue;

        /**
         * Detects types for keyword string by symbols. Enum order sensitive!
         *
         * @param keyword
         * @return
         */
        public static List<Type> getType(String keyword) {
            String kw = keyword;
            final List<Type> types = new ArrayList<>(2);
            for (Type t : values()) {
                if (kw.startsWith(t.getStartSymbol())) {
                    types.add(t);
                    if (t.isFinite()) {
                        return types;
                    } else {
                        kw = kw.substring(1);
                    }
                }
            }
            throw new IllegalArgumentException("Unreachable");
        }
    }
}
