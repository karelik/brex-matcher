package cz.ulman.brex;

import lombok.*;

import java.util.*;
import java.util.stream.*;

import static java.lang.Character.*;

/**
 * Collection of keywords
 */
@Getter
public class Keywords {
    private final List<Keyword> keywords;

    public Keywords(String... kwsArray){
        this(Arrays.asList(kwsArray));
    }
    /**
     * Main constructor, use string delimited by separator. Keywords are sorted immediately by importancy.
     * @param kwsCollection
     */
    // TODO protect duplicities and other mistakes in keywords input
    public Keywords(Collection<String> kwsCollection){
        if (kwsCollection == null || kwsCollection.isEmpty()) throw new IllegalArgumentException("No input keywords!");
        this.keywords = kwsCollection.stream()
                .map(Keyword::new).sorted(Comparator.comparing(Keyword::getSortValue).reversed()) // descending order
                .collect(Collectors.toList());
    }

    /**
     * Detects match of text by keywords. If only negative keywords are present and none is matched then returns true.
     * If there is at least one "positive" keyword and is not matched (and more negatives are not matched too) returns false.
     * @param text
     * @return
     */
    public boolean matches(String text, boolean clean){
        final String cleanText = clean ? cleanInput(text) : text;
        boolean somePositiveFail = false;
        for (Keyword k : keywords){
            final boolean negative = k.isNegative();
            if (k.matches(cleanText)){
                return !negative; // any negative match means false, any non-negative means success
            } else if (!negative){
                somePositiveFail = true;
            }
        }
        return !somePositiveFail;
    }

    public static String cleanInput(String word) {
        final StringBuilder str = new StringBuilder(word.length());
        for (Character ch : word.toCharArray()){
            if (isSpaceChar(ch) || isDigit(ch) || isAlphabetic(ch))
                str.append(ch);
            else {
                str.append(" ");
            }
        }
        return str.toString().replaceAll(" +", " ").trim();
    }

    /**
     * For better debugging
     * @return
     */
    public String toString(){
        final StringBuilder str = new StringBuilder("Keywords:\n");
        for (Keyword k : keywords){
            str.append(k).append("\n");
        }
        return str.toString();
    }
}
